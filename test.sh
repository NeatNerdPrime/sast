#!/bin/sh

# Make it TAP compliant, see http://testanything.org/tap-specification.html
echo "1..5"

failed=0
step=1

got="test/fixtures/gl-sast-report.json"
expect="test/expect/gl-sast-report.json"

groovy_build_file="test/fixtures/groovy/build.gradle"
cp ${groovy_build_file} ${groovy_build_file}.bak

restore_groovy_build_file() {
  cp ${groovy_build_file}.bak ${groovy_build_file}
}

# exclude "test" directory
export SAST_EXCLUDED_PATHS="ignored,excluded"

# Project found, artifact generated (bind mount)
desc="Generate expected artifact (bind mount, pull images)"
rm -f $got
PATH="/whatever" CI_PROJECT_DIR="$PWD/test/fixtures" ./sast

if test $? -eq 0 && diff -u $expect $got; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
step=$((step+1))
echo

# Project found, artifact generated (copy dir, no pull)
desc="Generate expected artifact (copy dir, no pull)"
rm -f $got
restore_groovy_build_file
PATH="/whatever" SAST_PULL_ANALYZER_IMAGES=0 ./sast "test/fixtures"

if test $? -eq 0 && diff -u $expect $got; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
step=$((step+1))
echo

# Project not compatible
desc="Exit with exit status 3 when no compatible analyzer can be found"
restore_groovy_build_file
SAST_DEFAULT_ANALYZERS="bandit,brakeman,gosec,spotbugs,flawfinder,phpcs-security-audit,security-code-scan,nodejs-scan,eslint,tslint,sobelow" CI_PROJECT_DIR="$PWD/test/fixtures/unknown" SAST_PULL_ANALYZER_IMAGES=0 ./sast

if [ $? -eq 3 ]; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
step=$((step+1))
echo

# Project empty
desc="Exit with exit status 4 when the project directory is completely empty"
mkdir -p "$PWD/test/fixtures/empty"
restore_groovy_build_file
CI_PROJECT_DIR="$PWD/test/fixtures/empty" SAST_PULL_ANALYZER_IMAGES=0 ./sast

if [ $? -eq 4 ]; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
step=$((step+1))
echo

# No vulnerabilities
desc="Returns an empty array when there are no vulnerabilities"
CI_PROJECT_DIR="$PWD/test/fixtures/sane" SAST_PULL_ANALYZER_IMAGES=0 ./sast

if grep '"vulnerabilities": \[\]' "$PWD/test/fixtures/sane/gl-sast-report.json" >/dev/null; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
step=$((step+1))
echo

# Finish tests
count=$((step-1))
if [ $failed -ne 0 ]; then
  echo "Failed $failed/$count tests"
  exit 1
else
  echo "Passed $count tests"
fi
